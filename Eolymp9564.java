import java.util.Scanner;

public class Eolymp9564 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int m = sc.nextInt();

        int[][] arr = new int[n][m];
        int[] sum = new int[n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arr[i][j] = sc.nextInt();
            }
        }


        for (int i = 0; i < n; i++) {
            int sumOfRow = 0;
            for (int j = 0; j < m; j++) {
                sumOfRow += arr[i][j];
                sum[i] += sumOfRow;
                sum[i] = sumOfRow;
            }
        }

        int max = sum[0];
        for (int i = 0; i < n; i++) {
            if (sum[i] > max) {
                max = sum[i];
            }
        }

        for (int i = 0; i < n; i++) {
            if (sum[i] == max) {
                System.out.print(i + 1 + " ");
            }
        }

    }
}
