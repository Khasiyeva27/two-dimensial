import java.util.Scanner;

public class Eolymp9565 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);

        int n=sc.nextInt();
        int m=sc.nextInt();

        int [][] arr=new int[n][m];

        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                arr[i][j]=sc.nextInt();
            }
        }

        int [] maxValues=new int[n];
        for(int i=0;i<n;i++){
            int max=arr[i][0];
            for(int j=1;j<m;j++){
                if(arr[i][j]>max){
                    max=arr[i][j];
                }
            }
            maxValues[i]=max;
        }

        int minOfMax=maxValues[0];
        for(int i=0;i<n;i++){
            if(maxValues[i]<minOfMax){
                minOfMax=maxValues[i];
            }
        }
        System.out.println(minOfMax);

    }
}
